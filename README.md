# Secure Datastore

TODO: Table of Contents
## Introduction

The purpose of this project is to create an API which provides a layer of security on top of an untrusted datastore. Regarding the file structure of the project, `SDS.go` represents this security API, while `datastore.go` represents the untrusted datastore. 

Attempting make meaningful security guarantees without control of the full stack is a difficult task. Thus, the following assumptions are made to allow for a more interseting conversation about the security of the system:

* Users will choose high-entropy passwords. This is enforced to some degree during user initialization, but nonetheless, brute-force attacks will be a secondary consideration.
* The utilized keystore is a trusted entity. Since this is a toy project, providing extensive PKI is unreasonable, and minimal security against an adversarial keystore is provided.

## User Initialization

User initialization is a simple process but has meaningful implications regarding the security of the rest of the system. It is worth noting that it is assumed that users will choose high-entropy passwords, with only some level of entropy explicitly enforced. Usernames are also limited to 16 characters, in order to be able to associate each user with a 16-byte UUID. For the sake of scalability, or for further assurance of collision resistance, the UUID size would have to be increased to 32 bytes. 

The first interesting step in user initialization is the generation of an "ephemeral" UUID. The goal of this ephemeral id is to handle a specific corner-case of user behavior. Including a random UUID in the key-generation process allows for the randomization of user key generation that is independent of their username and password combination. This is necessary to resist the "mine-laying" attacker who creates several "expected" profiles (i.e. username: jsmith, password: password), observes the placement of his own data into the datastore, deletes their profile, and then patiently waits to see the same segment of the datastore populated (confirming he now owns a valid pair of user credentials). Again, we are assuming user passwords are high-entropy, but this is a nice step toward not needing that assumption.

### Sources and Credits

TODO: Include link to Fall 2017 skeleton, etc.
TODO: source a couple of the golang things
