package SDS

func mediatedDatastoreSet(key UUID, value []byte) {
    DatastoreSet(key, value)
}

func mediatedDatastoreGet(key UUID) ([]byte, bool) {
    return DatastoreGet(key)
}

func mediatedDatastoreDelete(key UUID) {
    DatastoreDelete(key)
}

func mediatedKeystoreSet(key string, value PubKey) error {
    return KeystoreSet(key, value)
}

func mediatedKeystoreGet(key string) (value PubKey, ok bool) {
   return KeystoreGet(key)
}
