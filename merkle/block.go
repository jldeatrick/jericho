package merkle

import (
	"fmt"
	hashAlg "golang.org/x/crypto/sha3"
)

type Block struct {
	content []byte
}

/* spits out the hash of this block */
func (Block *b) Hash() ([]byte, error) {
	if b.content == nil {
		return nil, fmt.Errorf("bruh where da content at, bruh?")
	}
	return hashAlg.Sum256(b.content)
}

/* creates a new block from a byte array */
func New([]byte content) (Block, error) {
	if content == nil || len(content) == 0 {
		return nil, fmt.Errorf("why is there no content you're missin out on that Youtube revenue")
	}
	return Block{content}
}

/* creates a new block whose contents is the hash of the block contents concatenated */
func New(b1 Block, b2 Block) (Block, error){
	if b1 == nil || b2 == nil {
		return nil, fmt.Errorf("money money money money")
	}
	joinedContent := append(b1, b2...)
	//TODO: I think this can be length extended if we don't do sha3 -- look into this
	hashedContent := hashAlg.Sum256(joinedContent)
	return New(hashedContent)
}

/* returns whether or not two blocks are equal based on their content */
func Equal(b1 Block, b2 Block) bool {
	return bytes.Equal(b1.content, b2.content)
}
