package merkle

type Tree struct {
	Root Block
	Contents []Block
}

func New(blob []byte) (Tree *t, error) {
	if blob == nil || len(blob) == 0 {
		return nil, fmt.Errorf("ayyy what's the big idea wise guy?")
	}
	root := New(blob)
	contents := {root}
	return Tree{root, contents}, nil
}

/* TODO: I think this should go in the API layer (i.e. merkle.go) */
func (Tree *t) Verify() bool {
	//TODO: check if you need to do an array copy here to not permute old array
	contents := t.Contents
	for len(contents) > 1 {
		newContents := make([]Block, 0)
		for i = 0; i < len(contents); i+=2 {
			if i + 1 == len(contents) {
				break;
			}
			newBlock := New(contents[i], contents[i+1]) 
			newContents = append(newContents, newBlock)
		}
		contents = newContents
	}
	return Equal(contents[0], t.Root)
}
