package SDS

import (
	"testing"
)

func TestInit(t *testing.T) {

	t.Log("alice looks like this:", []byte("alice"), "!")
	t.Log("alicee looks like this:", []byte("alicee"), "!")
	t.Log("alice  looks like this:", []byte("alice "), "!")
	var b [7]byte
	t.Log("bytes  looks like this:", string(b[:]), "!")

	u, err := InitUser("alice","fubarrrrrrr")
	t.Log("initialized user was", u)
	if err != nil {
		t.Error("Failed to initialize user", err)
	}

	v, v_err := GetUser("alice", "fubarrrrrrr")
	t.Log("loaded user was", v)
	if (v_err != nil){
		t.Error("Error'd on correct password.")
	}

	test,test_err := GetUser("alice", "guess_pass")
	t.Log("loaded user was", test)
	if (test_err == nil){
		t.Error("failed to error on incorrect password")
	}

	test2,test_err2 := GetUser("alixe", "guess_pass")
	t.Log("loaded user was", test2)
	if (test_err2 == nil){
		t.Error("failed to error on incorrect username")
	}

	t.Error("You made it! But this is an easy way to print the logs :)")

}

// func TestInit(t *testing.T){
// 	t.Log("Initialization test")
// 	DebugPrint = true
// 	//someUsefulThings()

	// //DebugPrint = false
	// u, err := InitUser("alice","fubar")
	// if err != nil {
	// 	// t.Error says the test fails 
	// 	t.Error("Failed to initialize user", err)
	// }
	// // t.Log() only produces output if you run with "go test -v"
	// //t.Log("Got user", u)
	// // You probably want many more tests here.
	// v,_ := GetUser("alice", "fubar")
	// keygen := userlib.PBKDF2Key([]byte("alice"), []byte("fubar"), 256)
	// mp := userlib.DatastoreGetMap()

	// test,test_err := GetUser("alice", "guess_pass")
	// t.Log("loaded user was", test)
	// if (test_err == nil){
	// 	t.Error("failed to error on incorrect password")
	// }
	// test2,test_err2 := GetUser("alixe", "guess_pass")
	// t.Log("loaded user was", test2)
	// if (test_err2 == nil){
	// 	t.Error("failed to error on incorrect username")
	// }

// 	mp[string(keygen[64:96])] = randomBytes(len(mp[string(keygen[64:96])]))
// 	corrupt,corrupt_error := GetUser("alice", "fubar")
// 	t.Log("loaded user was", corrupt)
// 	if (corrupt_error == nil){
// 		t.Error("failed to error on corruption")
// 	}

// 	u, err = InitUser("alice","fubar")
// 	v,_ = GetUser("alice", "fubar")


// 	msg1 := []byte("haha funny boy")
// 	msg2 := []byte(" make funny scream")
// 	fullmsg := append(msg1, msg2...)

// 	v.StoreFile("filename", msg1)
// 	append_err := u.AppendFile("filename", []byte(msg2))
// 	if append_err != nil{
// 		t.Error("Failed to append", append_err)
// 	}
// 	file,load_err := v.LoadFile("filename")
// 	if load_err != nil{
// 		t.Error("Failed to load", load_err)
// 	}
// 	t.Log("original message", fullmsg)
// 	t.Log("output", file)
// 	t.Log("equality check", userlib.Equal(fullmsg, file))
// 	//println(string(file))
// }


// func TestStorage(t *testing.T){
// 	// And some more tests, because
// 	_, err := GetUser("alice", "fubar")

// 	if err != nil {
// 		t.Error("Failed to reload user", err)
// 		return
// 	}
// 	//t.Log("Loaded user", v)
// }

// func Test2(t *testing.T) {
// 	// Having previously created a user "alice" with password "fubar"...
// 	alice, _ := GetUser("alice", "fubar")
// 	also_alice, _ := GetUser("alice", "fubar")

// 	alice.StoreFile("todo", []byte("write tests"))
// 	todo, _ := also_alice.LoadFile("todo")
// 	if string(todo) != "write tests" {
// 		t.Error("Same user and password could not access file: ", todo)
// 	}

// }

// func TestShare(t *testing.T) {
// 	// Having previously created a user "alice" with password "fubar"...
// 	msg1 := []byte("this project ")
// 	msg2 := []byte("is ")
// 	msg3 := []byte("the worst")
// 	full_msg := append(append(append(append(append([]byte {}, msg1...), msg2...), msg3...), []byte("yo whatup carol")...), []byte(" whaddup bitches")...)

// 	t.Log("full message is", full_msg)


// 	alice, _ := GetUser("alice", "fubar")
// 	alice.StoreFile("fubar", msg1)

// 	InitUser("bob", "password")
// 	bob, _ := GetUser("bob", "password")

// 	InitUser("carol", "meme_queen")
// 	carol, _ := GetUser("carol", "meme_queen")

// 	bob.StoreFile("garp", []byte("this is the OG Garp"))
// 	ogfile,_ := bob.LoadFile("garp")
// 	t.Log("Loaded bob's OG file", ogfile)

// 	sharing, _ := alice.ShareFile("fubar", "bob")
// 	bob.ReceiveFile("garp", "alice", sharing)

// 	alice.AppendFile("fubar", msg2)
// 	bob.AppendFile("garp", msg3)

// 	sharing2,_ := alice.ShareFile("fubar", "carol")
// 	carol.ReceiveFile("memer_time", "alice", sharing2)

// 	bob.AppendFile("garp", []byte("yo whatup carol"))

// 	carol.AppendFile("memer_time", []byte(" whaddup bitches"))

// 	file,_ := alice.LoadFile("fubar")
// 	t.Log("Loaded alice's file", file)
// 	t.Log("equality check", userlib.Equal(full_msg, file))
// 	file2,_ := bob.LoadFile("garp")
// 	t.Log("Loaded bob's file", file2)
// 	t.Log("equality check", userlib.Equal(full_msg, file2))
// 	file3,_ := carol.LoadFile("memer_time")
// 	t.Log("Loaded carol's file", file3)
// 	t.Log("equality check", userlib.Equal(full_msg, file3))

// 	alice.RevokeFile("fubar")

// 	bob.AppendFile("garp", []byte("carol wtf"))
// 	bob.ReceiveFile("garp", "alice", sharing)
// 	carol.AppendFile("fubar", []byte("omg carol that was MY FILE!!!!"))
// 	bob.AppendFile("garp", []byte("carol wtf"))

// 	//bob.StoreFile("garp", []byte("oh well at least I can talk to myself"))


// 	file,_ = alice.LoadFile("fubar")
// 	t.Log("Loaded alice's file", file)
// 	t.Log("equality check", userlib.Equal(full_msg, file))
// 	file2,_ = bob.LoadFile("garp")
// 	t.Log("Loaded bob's file", file2)
// 	t.Log("equality check", userlib.Equal(full_msg, file2))
// 	file3,_ = carol.LoadFile("memer_time")
// 	t.Log("Loaded carol's file", file3)
// 	t.Log("equality check", userlib.Equal(full_msg, file3))


// }
