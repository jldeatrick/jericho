package SDS


/** 
 * Heavily modified version of the Berkeley Computer Security
 * course [CS-161] secure data store project. Skeleton developed 
 * by Fall 2017 course staff and Dr. Nicholas Weaver.
 * 
 * modifications made by Joe Deatrick. Links to skeleton included
 * in associated README along with security specifications. TODO
 *
 * The datastore is assumed to be untrusted and thus mediation
 * was performed at the local level
 * 
 * The Keystore, however, is a trusted entity.
 * 
 * Users are assumed to have strong passwords (some strength enforced).
 *
 */

import (
	"errors"
)

/**
 * Maximum length for potential usernames.
 * Should always be less than 16.
 */
const UNAME_MAX_LEN = 16

/* Minimum length for potential passwords. */
const PASSWD_STR = 8

/* Length (in bytes) for symmetric encryption keys. */
const ENC_KEY_LEN = 16

/* Length (in bytes) for authentication keys. */
const MAC_KEY_LEN = 16

/* User struct */
type User struct {
	Username string
	Password string

	/* RSA key pair for file sharing
	 * TODO: Upgrade user signature to ECDSA */
	PubKey *PubKey
	PrivKey *PrivKey
}

/**
 * File struct for sharing.
 * Contains block locations in datastore
 * And keys for AES and HMAC
 */
type SharingRecord struct {
	EncKey []byte
	AuthKey []byte
	FileLoc []byte
}

type FileInfo struct {
	BlockLocations [][]byte
	Root []byte
}

/* TODO: destroy user struct entry in database so username iv can be reused. */
func DeleteUser(user *User) {

	_,user_exists := mediatedKeystoreGet(user.Username)
	if !user_exists {
		panic("I honestly have no idea how this happened.")
	}

}

// It should store a copy of the userdata, suitably encrypted, in the
// datastore and should store the user's public key in the Keystore.

// The datastore may corrupt or completely erase the stored
// information, but nobody outside should be able to get at the stored
// User data: the name used in the datastore should not be guessable
// without also knowing the password and username.

// You are not allowed to use any global storage other than the
// Keystore and the datastore functions in the  library.

// You can assume the user has a STRONG password
// TODO: add Debug messages
func InitUser(username string, password string) (*User, error){

    var user *User = new(User)

    /* If user already exists, don't re-init */
    _, user_exists := mediatedKeystoreGet(username)
    if user_exists {
        return nil, errors.New("User already exists.")
    }

    user.Username = username
    user.Password = password

    /* Attach RSA keys to the user, panic on failure. */
    attachKeys(user)
    mediatedKeystoreSet(username, *user.PubKey)

    /* Spin up keys and location for the private key, filemap */
    key_enc, key_mac, key_loc := Argon2KDF(stitch(username, password),
                                           []byte("artist hobby luck"))
    fmap_enc, fmap_mac, fmap_loc := Argon2KDF(stitch(username, password),
                                               []byte("safe jug dragon"))

    /* Store user RSA keys and file map in the datastore. */
    storeObject(key_enc, key_mac, key_loc, *user.PrivKey)
    storeObject(fmap_enc, fmap_mac, fmap_loc, make(map[string]UUID))

    return user, nil
}

func storeObject(enc_key []byte, mac_key []byte, loc []byte, obj interface{}) {
    bytes := marshal(obj)
    payload := EncryptAndMac(enc_key, mac_key, bytes)
    mediatedDatastoreSet(bytesToUUID(loc), payload)
}

func requestObject(enc_key []byte, mac_key []byte, loc []byte, typ Marshal_t) interface{} {
    payload, ok := mediatedDatastoreGet(bytesToUUID(loc))
    if !ok { panic("fetching payload failed.") }
    plaintext := DecryptAndVerify(enc_key, mac_key, payload)
    obj := unmarshal(plaintext, typ)
    return obj
}

// This fetches the user information from the Datastore.  It should
// fail with an error if the user/password is invalid, or if the user
// data was corrupted, or if the user can't be found.
func GetUser(username string, password string) (*User, error) {
    var userdataptr *User = new(User)

    /* If user doesn't exist, throw error */
    _, user_exists := mediatedKeystoreGet(username)
    if !user_exists {
        return nil, errors.New("User does not exist.")
    }

    userdataptr.Username = username
    userdataptr.Password = password

    /* get RSA keys. Defer File Map fetch to when we start storing files */
    key_enc, key_mac, key_loc := Argon2KDF(stitch(username, password),
                                           []byte("artist hobby luck"))
    privkey := requestObject(key_enc, key_mac, key_loc, PRIVATE_KEY).(PrivKey)

    userdataptr.PrivKey = &privkey
    userdataptr.PubKey = &privkey.PublicKey

    return userdataptr, nil
}

//func fetch() []byte, error {}

// This stores a file in the datastore.
//
// The name of the file should NOT be revealed to the datastore!
func (userdata *User) StoreFile(filename string, data []byte) error {

	/* get, decrypt, verify the file map and panic if we can't find it */
    fmap_enc, fmap_mac, fmap_loc := Argon2KDF(stitch(userdata.Username, userdata.Password),
                                              []byte("safe jug dragon"))

    fmap :=  requestObject(fmap_enc, fmap_mac, fmap_loc, FILE_MAP).(map[string]UUID)

    /* encrypt, and store data blob with new keys and location */
    new_enc, new_mac, data_loc, file_loc := RandomBytes(ENC_KEY_LEN),			                                             RandomBytes(MAC_KEY_LEN),
                                             RandomBytes(16), RandomBytes(16)

    /* TODO: Change the flow to allow us to only encrypt here, without marshalling */
    storeObject(new_enc, new_mac, data_loc, data)

    /*
     * create, encrypt, mac, store new file struct.
     * TODO: fix Merkle Tree Root
     */
    file := FileInfo{[][]byte{data_loc}, data}

    storeObject(new_enc, new_mac, file_loc, file)

    /* create, enc, mac, store sharing record */
    record := SharingRecord{new_enc, new_mac, file_loc}
    rec_enc, rec_mac, rec_loc := Argon2KDF(stitch(userdata.Username, userdata.Password,
                                            filename), []byte("destiny cup book"))
    storeObject(rec_enc, rec_mac, rec_loc, record)

    /* record where the sharing record is */
    fmap[filename] = bytesToUUID(rec_loc)

    /* re-encrypt, re-mac and store the filemap */
    storeObject(fmap_enc, fmap_mac, fmap_loc, fmap)

    return nil
}


// This adds on to an existing file.
//
// Append should be efficient, you shouldn't rewrite or reencrypt the
// existing file, but only whatever additional information and
// metadata you need.

func (userdata *User) AppendFile(filename string, data []byte) (err error){
	return nil
}

// This loads a file from the Datastore.
//
// It should give an error if the file is corrupted in any way.
func (userdata *User) LoadFile(filename string)(data []byte, err error) {
	return nil, nil
}

// This creates a sharing record, which is a key pointing to something
// in the datastore to share with the recipient.

// This enables the recipient to access the encrypted file as well
// for reading/appending.

// Note that neither the recipient NOR the datastore should gain any
// information about what the sender calls the file.  Only the
// recipient can access the sharing record, and only the recipient
// should be able to know the sender.

func (userdata *User) ShareFile(filename string, recipient string)(
	msgid string, err error){
		return "nil", nil

}


// Note recipient's filename can be different from the sender's filename.
// The recipient should not be able to discover the sender's view on
// what the filename even is!  However, the recipient must ensure that
// it is authentically from the sender.
func (userdata *User) ReceiveFile(filename string, sender string,
	msgid string) error {
		return nil
}

// Removes access for all others.  
func (userdata *User) RevokeFile(filename string) (err error){
	return nil
}
