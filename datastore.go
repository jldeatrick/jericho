package SDS

import (
	"errors"
	"github.com/google/uuid"
)


type UUID = uuid.UUID

// Datastore and Keystore variables
var datastore map[UUID][]byte = make(map[UUID][]byte)
var keystore map[string]PubKey = make(map[string]PubKey)

/*
********************************************
**           Datastore Functions          **
**       DatastoreSet, DatastoreGet,      **
**     DatastoreDelete, DatastoreClear    **
********************************************
*/

// Sets the value in the datastore
func DatastoreSet(key UUID, value []byte) {
	foo := make([]byte, len(value))
	copy(foo, value)

	datastore[key] = foo
}

// Returns the value if it exists
func DatastoreGet(key UUID) (value []byte, ok bool) {
	value, ok = datastore[key]
	if ok && value != nil {
		foo := make([]byte, len(value))
		copy(foo, value)
		return foo, ok
	}
	return
}

// Deletes a key
func DatastoreDelete(key UUID) {
	delete(datastore, key)
}

// Use this in testing to reset the datastore to empty
func DatastoreClear() {
	datastore = make(map[UUID][]byte)
}

// Use this in testing to reset the keystore to empty
func KeystoreClear() {
	keystore = make(map[string]PubKey)
}

// Sets the value in the keystore
func KeystoreSet(key string, value PubKey) error {
	_, present := keystore[key]
	if present != false {
		return errors.New("That entry in the Keystore has been taken.")
	}

	keystore[key] = value
	return nil
}

// Returns the value if it exists
func KeystoreGet(key string) (value PubKey, ok bool) {
	value, ok = keystore[key]
	return
}

// Use this in testing to get the underlying map if you want
// to play with the datastore.
func DatastoreGetMap() map[UUID][]byte {
	return datastore
}

// Use this in testing to get the underlying map if you want
// to play with the keystore.
func KeystoreGetMap() map[string]PubKey {
	return keystore
}
