package SDS

import (
	"fmt"
	"log"
	"strings"
	"time"

	"io"
	"crypto/rand"
	"encoding/json"
	"github.com/google/uuid"
)

type Marshal_t int
const (
    PRIVATE_KEY    Marshal_t = iota + 1
    FILE_MAP
    SHARING_RECORD
    FILE_INFO
)

// Debug print true/false
var DebugPrint = false

// DebugMsg. Helper function: Does formatted printing to stderr if
// the DebugPrint global is set.  All our testing ignores stderr,
// so feel free to use this for any sort of testing you want.

func SetDebugStatus(status bool){
	DebugPrint = status
}

func DebugMsg(format string, args ...interface{}) {
	if DebugPrint {
		msg := fmt.Sprintf("%v ", time.Now().Format("15:04:05.00000"))
		log.Printf(msg+strings.Trim(format, "\r\n ")+"\n", args...)
	}
}

/**
 * Stitch together multiple objects as byte arrays 
 * Currently works with strings, byte arrays, and uuids
 */
func stitch(args ...interface{}) []byte {
	curr := make([]byte, 0)
	for _, obj := range args {
		switch obj.(type) {
		  case string:
		  	s := obj.(string)
		    curr = append(curr, []byte(s)...)
		  case []byte:
		  	b := obj.([]byte)
		  	curr = append(curr, b...)
		  case uuid.UUID:
		  	u := obj.(uuid.UUID)
		  	curr = append(curr, u[:]...)
		  default:
		    panic("unsupported type.")
		}
	}
	return curr
}

// RandomBytes. Helper function: Returns a byte slice of the specificed
// size filled with random data
func RandomBytes(bytes int) (data []byte) {
	data = make([]byte, bytes)
	if _, err := io.ReadFull(rand.Reader, data); err != nil {
		panic(err)
	}
	return
}

// Helper function: Takes the first 16 bytes and
// converts it into the UUID type
func bytesToUUID(data []byte) (ret uuid.UUID) {
	for x := range(ret){
		ret[x] = data[x]
	}
	return
}

func pad(arr []byte, size int) []byte {
	ret := arr
	for len(ret) < size {
		var null_byte [1]byte
		ret = append(ret, null_byte[:]...)
	}
	return ret
}

func strToUUID(str string) uuid.UUID {
	str_bytes := []byte(str)
	padded_bytes := pad(str_bytes, 16)
	return bytesToUUID(padded_bytes)
}

/* Wrapper for json.marshal which does panicking */
func marshal(class interface{}) []byte {
	obj, err := json.Marshal(class)
	if err != nil {
		panic("JSON marhsalling failed.")
	}
	return obj
}

/* Wrapper for json.unmarshal for PrivKeys and Filemaps which does panicking. */
func unmarshal(payload []byte, umtype Marshal_t) interface{} {
	switch umtype {
	  case PRIVATE_KEY:
                ret := PrivKey{}
                err := json.Unmarshal(payload, &ret)
		if err != nil { panic("JSON unmarhsalling failed.") }
		return ret
	  case FILE_MAP:
                ret := make(map[string]UUID)
                err := json.Unmarshal(payload, &ret)
		if err != nil { panic("JSON unmarhsalling failed.") }
		return ret
	  default:
	    panic("unsupported type for unmarhsalling.")
	}
}
