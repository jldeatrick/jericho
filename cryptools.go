package SDS

import (
	"crypto"
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/rand"
	"crypto/rsa"
	"golang.org/x/crypto/sha3"
	"errors"
	"golang.org/x/crypto/argon2"
	"github.com/google/uuid"
)

/* RSA key size */
const RSA_KEY_LEN = 2048

/* AES block size */
const AES_BLK_LEN = aes.BlockSize

type Payload struct {
	Iv []byte
	Enc_data []byte
	Auth_data []byte
}

/*
********************************************
**         Public Key Encryption          **
**       PKEKeyGen, PKEEnc, PKEDec        **
********************************************
*/

type PubKey = rsa.PublicKey
type PrivKey = rsa.PrivateKey

// Generates a key pair for public-key encryption via RSA
func RSAKeyGen() (*PubKey, *PrivKey, error) {
	PrivKey, err := rsa.GenerateKey(rand.Reader, RSA_KEY_LEN)

	return &PrivKey.PublicKey, PrivKey, err
}

/* Wrapper for RSAKeyGen with panicking */
func attachKeys(user *User) {
    pubKey, privKey, err := RSAKeyGen()

    if err != nil {
        panic("User key generation failed.")
    }

    user.PubKey, user.PrivKey = pubKey, privKey
}

func encryptRSA(pubkey *PubKey, plaintext []byte) ([]byte, error) {
	return rsa.EncryptOAEP(sha3.New256(), rand.Reader, pubkey, plaintext, nil)
}

func decryptRSA(privkey *PrivKey, ciphertext []byte) ([]byte, error) {
	return rsa.DecryptOAEP(sha3.New256(), rand.Reader, privkey, ciphertext, nil)
}

func signRSA(privkey *PrivKey, message []byte) ([]byte, error) {
	hashed := sha3.Sum256(message)
	return rsa.SignPKCS1v15(rand.Reader, privkey, crypto.SHA3_256, hashed[:])
}

func verifyRSA(pubkey *PubKey, message []byte, signature []byte) error {
	hashed := sha3.Sum256(message)
	return rsa.VerifyPKCS1v15(pubkey, crypto.SHA3_256, hashed[:], signature)
}


/*
********************************************
**                HMAC                    **
**         HMACEval, HMACEqual            **
********************************************
*/

// Evaluate the HMAC using sha3
func HMACEval(key []byte, msg []byte) []byte {
	if len(key) != 16 && len(key) != 24 && len(key) != 32 {
		panic(errors.New("The input as key for HMAC should be a 16-byte key."))
	}

	mac := hmac.New(sha3.New256, key)
	mac.Write(msg)
	res := mac.Sum(nil)

	return res
}

// Equals comparison for hashes/MACs
// Does NOT leak timing.
func HMACEqual(a []byte, b []byte) bool {
	return hmac.Equal(a, b)
}


/*
********************************************
**               KDF                      **
**            Argon2Key                   **
********************************************
*/

// Argon2:  Automatically choses a decent combination of iterations and memory
// Use this to generate a key from a password
func Argon2Key(password []byte, salt []byte, keyLen uint32) []byte {
	return argon2.IDKey(password, salt, 1, 64*1024, 4, keyLen)
}


/*
********************************************
**        Symmetric Encryption            **
**           SymEnc, SymDec               **
********************************************
*/

/* source: https://golang.org/pkg/crypto/cipher/#example_NewCFBEncrypter */
func encryptCFB(key []byte, iv []byte, plaintext []byte) []byte {

	/* TODO: null checks */

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	ciphertext := make([]byte, len(plaintext))

	stream.XORKeyStream(ciphertext, plaintext)

	return ciphertext
}

/* source: https://golang.org/pkg/crypto/cipher/#example_NewCFBDecrypter */
func decryptCFB(key []byte, iv []byte, ciphertext []byte) []byte {

	/* TODO: null checks */

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	stream := cipher.NewCFBDecrypter(block, iv)
	plaintext := make([]byte, len(ciphertext))

	stream.XORKeyStream(plaintext, ciphertext)

	return plaintext
}

func Argon2KDF(keygen_material []byte, salt []byte) ([]byte, []byte, []byte) {
	keygen := Argon2Key(keygen_material, salt, ENC_KEY_LEN + MAC_KEY_LEN)
	encrypt_key := keygen[:ENC_KEY_LEN]
	auth_key := keygen[ENC_KEY_LEN:ENC_KEY_LEN+MAC_KEY_LEN]
	location := keygen[ENC_KEY_LEN+MAC_KEY_LEN:]

	return encrypt_key, auth_key, location
}

func Encrypt(encrypt_key []byte, plaintext []byte) []byte {
	return RandomBytes(16)
}

func EncryptAndMac(encrypt_key []byte, auth_key []byte, plaintext []byte) []byte {

	iv := uuid.New()

	ciphertext := encryptCFB(encrypt_key, iv[:], plaintext)
	encrypted_payload := append(iv[:], ciphertext...)
	authenticated_payload := HMACEval(auth_key, plaintext)

	/* Generate and return final payload */
	return append(encrypted_payload, authenticated_payload...)
}

func DecryptAndVerify(encrypt_key []byte, auth_key []byte, payload []byte) []byte {

	iv := payload[:16]
	ciphertext := payload[16:len(payload) - 32]
	mac := payload[len(payload) - 32:]

	plaintext := decryptCFB(encrypt_key, iv[:], ciphertext)

	auth_payload := HMACEval(auth_key, plaintext)

	/* authenticate */
	if !HMACEqual(auth_payload, mac) {
		panic(errors.New("HMAC could not be verified."))
	}

	/* Generate and return final payload */
	return plaintext

}

